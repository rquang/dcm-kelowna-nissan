// date picker
jQuery(function() {
	jQuery( ".date-picker" ).datepicker({ // date picker settings
		showOn: "button",
		buttonImage: "/dcm-images/components/default/leads/date-picker.png",
		buttonImageOnly: true,
		buttonText: "Select Date",
		dateFormat: "yy-mm-dd"
	});	
});